# LUMEN-DOCKER-OCI

Este es un proyecto de lumen con oci8 configurado

    
# Entorno desarrollo LUMEN-DOCKER-OCI Docker

Documentación necesaria para montar tu ambiente de  LUMEN-DOCKER-OCI utilizando Docker

### ¿Que necesitas?

* Docker version 19.03.1, build 74b1e89
* Git 2.22.0

### Configurar el proyecto

1.  Clona el repositorio 


```sh
https://gitlab.com/samuel005/lumen-docker-oci8.git
```

2. Ahora vamos a levantar el ambiente con Docker

```sh
# Accede al directorio lumen-docker-oci8
cd lumen-docker-oci8

# Despliega el proyecto con Docker
docker-compose up -d --build
```

3.  Ahora vamos a configurar acceso a la ruta de logs.

```sh
# Vamos a setear los archivos de configuración del projecto (accesos a logs, constantes, etc.)

# Configuraciones generales
cp .env.sample .env

# Acceso a la carpeta de logs (Esto si no tiene acceso a la carpeta de logs)
chmod -R o+w storage

#Acceder al contedor y instalar dependencias
docker exec -it lumen-oci8-php sh 
composer install

```

> ### Listo! 🚀
> Accede a AIR-GATEWAY en [http://localhost:8060](http://localhost:8060) 

> Accede al Logs de AIR-GATEWAY en [http://localhost:8060/logs](http://localhost:8060) 

> Si el contenedor no sube, verifica que el puerto 8060 no esté siendo utilizado
```sh
# MacOS o GNU/Linux 🤩
sudo lsof -i -n -P | grep 8060

# Windows 😒
netstat -an
```

### Configuración de Git para desarrolladores

```sh
# Configurar tu nombre y correo de Git
git config user.email "username@viva.com.do"
git config user.name "Your name"

# Evitar que Git versiones los permisos de los archivos
git config core.filemode false

# (solo si estás en Windows). Decirle a git que ignore los line endings
git config core.autocrlf false

# Vamos a configurar Git para que haga prune de nuetras ramas automaticamente
git config fetch.prune true
```

### Información importante

Este proyecto esta divido en dos contenedores unos para el php y el otro para el apache.

    * lumen-oci8-apache
    * lumen-oci8-php

Posee una vista para los logs en la ruta   http://{proyecto}/logs


#### Documentacion Oficial

La documentación del Framework la puedes encontrar en   [Lumen](https://lumen.laravel.com/docs).

#### Pruebas Unitarias
Ejecutar las pruebas unitarias:

    docker exec -it lumen-oci8-php sh 
    cd tests/
    vendor/bin/phpunit
    
    OK (3 tests, 31 assertions)


#### License

`© Samuel Paez`

